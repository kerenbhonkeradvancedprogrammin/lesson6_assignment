﻿namespace WindowsFormsApp1
{
    partial class bdays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.bdayMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // calendar
            // 
            this.calendar.Location = new System.Drawing.Point(129, 18);
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 1;
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // bdayMsg
            // 
            this.bdayMsg.AutoSize = true;
            this.bdayMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bdayMsg.Location = new System.Drawing.Point(107, 189);
            this.bdayMsg.Name = "bdayMsg";
            this.bdayMsg.Size = new System.Drawing.Size(80, 16);
            this.bdayMsg.TabIndex = 2;
            this.bdayMsg.Text = "pick a day";
            this.bdayMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bdayMsg.Click += new System.EventHandler(this.bdayMsg_Click);
            // 
            // bdays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 261);
            this.Controls.Add(this.bdayMsg);
            this.Controls.Add(this.calendar);
            this.Name = "bdays";
            this.Text = "bdays";
            this.Load += new System.EventHandler(this.bdays_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.Label bdayMsg;
    }
}