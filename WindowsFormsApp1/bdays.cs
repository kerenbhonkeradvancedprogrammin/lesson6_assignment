﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Threading;

namespace WindowsFormsApp1
{
    public partial class bdays : Form
    {

        private string _userName;
        private Dictionary<string, string> _birthdays;

        public bdays()
        {
            InitializeComponent();
        }

        public string Username
        {
            get { return _userName; }
            set { _userName = value;}
        }

        private void bdays_Load(object sender, EventArgs e)
        {
            string fileName = _userName + "BD.txt";     //for example: KerenBD.txt
            FileStream fs = File.Open(fileName, FileMode.OpenOrCreate); //if the file isn't exists, create a new one
            fs.Close();
            string[] lines = System.IO.File.ReadAllLines(fileName);     //read all the lines in the birthdays' file
            _birthdays = new Dictionary<string, string>();      //the key = the date (because on every date there is only one person who celebrates
                                                                //the value = the name

            for(int i = 0; i < lines.Length; i++)
            {
                //every line in the birthdays' file looks like this: name,birthday, for example: "Keren,05/03/2001"
                int comma = lines[i].IndexOf(",");
                //separate between the name and the date
                string name = lines[i].Substring(0, comma);
                string date = padding(lines[i].Substring(comma + 1, 5));
                _birthdays.Add(date, name);     //add to the dictionary
            }
        }

        private string padding(string date)
        {
            //separate the day, month and year
            string[] dateParts = date.Split('/');

            //pad the day and the month = add zero in blank places
            //for example: 5/3/2001 => 05/03/2001
            dateParts[0] = (dateParts[0].Length < 2) ? "0" + dateParts[0] : dateParts[0];
            dateParts[1] = (dateParts[1].Length < 2) ? "0" + dateParts[1] : dateParts[1];
 
            return dateParts[0] + "/" + dateParts[1];   //returns the day and the month
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            // Change culture to en-US
            //so the date will be like this: mm/dd/yyyy
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            string birthday = e.Start.ToShortDateString();
            string date = padding(birthday);
            
            if(_birthdays.ContainsKey(date))    //if the chosen date is in the birthdays' list
            {
                bdayMsg.Text = _birthdays[date] + " celebrates birthday on " + birthday;
            }
            else
            {
                bdayMsg.Text = "No one celebrates birthday on " + birthday;
            }
        }

        private void bdayMsg_Click(object sender, EventArgs e)
        {

        }
    }
}
