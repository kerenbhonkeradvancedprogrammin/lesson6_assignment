﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void enter_Click(object sender, EventArgs e)
        {
            String username = this.userName.Text;   //get the username, for example: "keren"
            String password = this.password.Text;   //get the password, for example: "123456"
            String usernameNPassword = username + "," + password;   //connect them, for example: "keren,123456"

            string[] lines = System.IO.File.ReadAllLines(@"Users.txt"); //read all the lines in the users' file
            if(lines.Contains(usernameNPassword))   //if the username and password that the user wrote are in the file
            {
                this.Hide();
                bdays f = new bdays();  //new window
                f.Username = username;
                f.ShowDialog();
            }
            else
            {
                MessageBox.Show("Wrong username or password");
            }

        }
    }
}
